import { Button, CenterView } from "../components";
import { Link, useLoaderData } from "react-router-dom";
const End = () => {
  const { content, actions } = useLoaderData();
  return (
    <CenterView>
      <div className="w-3/4 text-justify flex-col flex justify-center items-center gap-y-6">
        <div className="flex flex-col items-center">
          {content.map((p) => p)}
        </div>
        <div className="flex-col flex justify-center items-center gap-y-2">
          {actions.map((action) => (
            <Link to={action.goTo} key={action.text}>
              <Button>{action.text}</Button>
            </Link>
          ))}
        </div>
      </div>
    </CenterView>
  );
};

export default End;
