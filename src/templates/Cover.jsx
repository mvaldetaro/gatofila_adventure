import { Button, CenterView } from "../components";
import { Link, useLoaderData } from "react-router-dom";
const Main = () => {
  const { actions } = useLoaderData();

  return (
    <CenterView>
      <div className="flex-col flex justify-center items-center gap-y-10">
        <div className="flex flex-col items-center">
          <h1 className="text-3xl font-bold underline text-center mb-2">
            A Pequena <br />
            Valquíria Gatófila
          </h1>
          <p className="text-center">
            Um conto baseado em fatos reais
            <br />
            <span className="line-through">ou nem tanto.</span>
          </p>
        </div>
        <div className="flex-col flex justify-center items-center gap-y-2">
          {actions.map((action) => (
            <Link to={action.goTo} key={action.text}>
              <Button>{action.text}</Button>
            </Link>
          ))}
          <p>
            <small>(Botão grande em caso de sono)</small>
          </p>
        </div>
      </div>
    </CenterView>
  );
};

export default Main;
