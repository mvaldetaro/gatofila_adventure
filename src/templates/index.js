export { default as Cover } from "./Cover";
export { default as Page } from "./Page";
export { default as Quest } from "./Quest";
export { default as Chapter } from "./Chapter";
export { default as BadFinal } from "./BadFinal";
export { default as GreatFinal } from "./GreatFinal";
export { default as End } from "./GreatFinal";
