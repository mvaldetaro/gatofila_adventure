import { Button, CenterView } from "../components";
import { Link, useLoaderData } from "react-router-dom";

const Chapter = () => {
  const { actions } = useLoaderData();
  return (
    <CenterView>
      <div className="flex-col flex justify-center items-center gap-y-6">
        <div className="flex flex-col items-center">
          <h1 className="text-1xl font-bold underline">Capítulo 9 3/4:</h1>
          <h1 className="text-3xl font-bold underline">Feliz Aniversário!</h1>
        </div>
        <div className="flex-col flex justify-center items-center gap-y-2">
          {actions.map((action) => (
            <Link to={action.goTo} key={action.text}>
              <Button>{action.text}</Button>
            </Link>
          ))}
        </div>
      </div>
    </CenterView>
  );
};

export default Chapter;
