import { Container, BorderedWarp } from "./components";
import { RouterProvider } from "react-router-dom";
import routers from "./data/routers";

// const router = createBrowserRouter(routers);
function App() {
  return (
    <Container>
      <BorderedWarp>
        <RouterProvider router={routers} />
      </BorderedWarp>
    </Container>
  );
}

export default App;
