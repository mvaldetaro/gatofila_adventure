import { PropTypes } from "prop-types";
import React, { memo } from "react";

// Context
const AdventureContext = React.createContext();

const initialState = {
  leia: false,
  petiscos: false,
  dragon: "", //convenceu, enfrentou, deu petiscos
};

const dragon = ["convenceu", "enfrentou", "petiscos"];

// Reducers
const reducers = (state = initialState, action) => {
  if (action.type === "LEIA_CHANGED") {
    return {
      ...state,
      leia: action.payload,
    };
  }
  if (action.type === "PETISCOS_CHANGED") {
    return {
      ...state,
      leia: action.payload,
    };
  }
  if (action.type === "DRAGON_CHANGED") {
    return {
      ...state,
      leia: action.payload,
    };
  }
};

// Dispatchs
const updateLeia = (pString) => ({
  type: "TITLE_CHANGED",
  payload: pString,
});

// Custom Hook
export const useAdventureContext = () => React.useContext(AdventureContext);

export const AdventureProvider = memo(({ children }) => {
  const [state, dispatch] = React.useReducer(reducers, initialState);

  const handleUpdateLeia = (pValue) => {
    dispatch(updateLeia(pValue));
  };

  const enums = {
    dragon,
  };

  return (
    <AdventureContext.Provider
      value={{ state, enums, actions: { handleUpdateLeia } }}
    >
      {children}
    </AdventureContext.Provider>
  );
});

AdventureProvider.displayName = "AdventureProvider";
AdventureProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export default AdventureProvider;
