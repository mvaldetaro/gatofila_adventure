import { createBrowserRouter } from "react-router-dom";
import {
  Cover,
  Page,
  Quest,
  Chapter,
  BadFinal,
  GreatFinal,
  End,
} from "../templates";

const routers = createBrowserRouter([
  // Cover
  {
    path: "/",
    element: <Cover />,
    loader: () => {
      return { actions: [{ text: "Toque para iniciar", goTo: "/intro" }] };
    },
  },
  // Intro
  {
    path: "/intro",
    element: <Page />,
    loader: () => {
      return {
        content: [
          <p className="mb-4" key="intro-a-1">
            Era uma vez, a muito tempo atrás, numa galaxia muito distante numa
            toca no chão vivia uma bruxinha cheia de encanto e magia. Não uma
            toca suja e úmida, cheia de restos de minhoca e cheiro de loudo,
            tampouco uma toca seca, vazia e arenosa, sem nada para sentar ou
            comer: era a toca de uma pequena bruxinha gatófila e isso quer dizer
            aconchego.
          </p>,
          <p className="mb-4" key="intro-a-2">
            Uma toca rodeada por feitiços, poções sem glutém, lápis e pincéis de
            todas as cores. Pinturas e desenhos em todas as paredes, uma cama
            grande, onde caberia 42 dela e uma gata peluda com muita
            personalidade chamada Leia.
          </p>,
        ],
        actions: [{ text: "Continuar", goTo: "/intro-pt-2" }],
      };
    },
  },
  // Intro - Parte 2
  {
    path: "/intro-pt-2",
    element: <Page />,
    loader: () => {
      return {
        content: [
          <p className="mb-4" key="intro-b-1">
            Mah, a bruxinha gatófila, está prestes a comemorar seu 33º
            aniversário. Ela foi adotada por um bando de gatos ainda muito
            jovem, lhe dando diversas habilidades gatiferas, como: julgar com
            olhar, mau humor quando está com fome, dormir bem-bem-bem
            dobradinha, dormir por longas horas, dormir após comer e dormir após
            dormir. Enquanto os preparativos para a comemoração começam, ela é
            chamada por Merodin, o Pai de Todos Fura Bolo Mata Piolho, para uma
            missão em seu dia especial.
          </p>,
        ],
        actions: [{ text: "Continuar", goTo: "/chapter" }],
      };
    },
  },
  // Capítulo 1
  {
    path: "/chapter",
    element: <Chapter />,
    loader: () => {
      return {
        actions: [{ text: "Continuar", goTo: "/step-1" }],
      };
    },
  },
  // Passo 1
  {
    path: "/step-1",
    title: "O Chamado da Aventura",
    element: <Quest />,
    loader: () => {
      return {
        content: [
          <p className="mb-4" key="step-1">
            Enquanto os convidados chegam, Merodin aparece em um relâmpago e
            revela que Mah deve embarcar em uma jornada para provar sua
            valentia. No entanto, Merodin sabe do amor de Mah por gatos e
            comida. Ela pode começar sua missão agora ou...
          </p>,
        ],
        actions: [
          {
            text: "Pedir a Merodin se ela pode levar sua gata de estimação consigo na jornada.", // Step 2 A
            goTo: "/step-2-a",
          },
          {
            text: "Aceitar a missão imediatamente e partir, mas pedir a Merodin por alguns petiscos nórdicos para a viagem.", // Step 2 B
            goTo: "/step-2-b",
          },
          {
            text: "Recusar o chamado de Merodin e continuar dormindo, afinal, é seu aniversário e ela gosta de dormir até tarde.", //  Final 4
            goTo: "/final-3",
          },
        ],
      };
    },
  },
  // Passo 2
  {
    path: "/step-2-a",
    element: <Quest />,
    title: "A Floresta Sombria",
    loader: () => {
      return {
        content: [
          <p className="mb-4" key="step-2-a">
            Mah segue as instruções de Merodin e entra em uma floresta escura,
            cheia de magia e mistérios. Ela encontra um espírito da floresta que
            conhece sua paixão por gatos e magia. Ele a desafia a...
          </p>,
        ],
        actions: [
          {
            text: '[Poder da Companhia] Mah e Leia utilizam o "Olhar do Julgamento" expulsando o espírito para as profundezas da floresta.',
            goTo: "/step-3-b",
          },
        ],
      };
    },
  },
  {
    path: "/step-2-b",
    element: <Quest />,
    title: "A Floresta Sombria",
    loader: () => {
      return {
        content: [
          <p className="mb-4" key="step-2-a">
            Mah segue as instruções de Merodin e entra em uma floresta escura,
            cheia de magia e mistérios. Ela encontra um espírito da floresta que
            conhece sua paixão por gatos e magia. Ele a desafia a...
          </p>,
        ],
        actions: [
          {
            text: "Jogar todas poções e feitiços desesperadamente.", // Step 3
            goTo: "/step-3-a",
          },
          {
            text: "Negociar com um gato feiticeiro que a ajudará se ela prometer compartilhar algumas de suas poções especiais.", // Step 3
            goTo: "/step-3-a",
          },
          {
            text: "Voltar pra casa, sem paciência pra isso.", // Final 4
            goTo: "/final-2",
          },
        ],
      };
    },
  },
  // Passo 3
  {
    path: "/step-3-a",
    element: <Quest />,
    title: "O Desafio Matinal da Bruxinha",
    loader: () => {
      return {
        content: [
          <p className="mb-4" key="step-3-a">
            Após superar o desafio na floresta, Mah chega a uma ponte guardada
            por um dragão poderoso. No entanto, o sol está alto no céu, e Mah é
            conhecida por ser menos poderosa de manhã. Ela pode...
          </p>,
        ],
        actions: [
          {
            text: "[Petisquinhos] Oferecer ao dragão um pouco dos petiscos nórdicos em troca de um caminho seguro.",
            goTo: "/step-4-a",
          },
        ],
      };
    },
  },
  {
    path: "/step-3-b",
    element: <Quest />,
    title: "O Desafio Matinal da Bruxinha",
    loader: () => {
      return {
        content: [
          <p className="mb-4" key="step-3-b">
            Após superar o desafio na floresta, Mah chega a uma ponte guardada
            por um dragão poderoso. No entanto, o sol está alto no céu, e Mah é
            conhecida por ser menos poderosa de manhã. Ela pode...
          </p>,
        ],
        actions: [
          {
            text: "Lutar contra o dragão em um duelo mágico, apesar de sua indisposição matinal.",
            goTo: "/step-4-a",
          },
          {
            text: "Usar sua astúcia para encontrar uma maneira de adiar a batalha até a tarde usando encantamentos temporais.",
            goTo: "/step-4-a",
          },
          {
            text: "Dá meia volta, retorna pra casa e fica dormindo até tarde.", // Final 4",
            goTo: "/final-2",
          },
        ],
      };
    },
  },
  // Passo 4
  {
    path: "/step-4-a",
    element: <Quest />,
    title: "A Conclusão da Jornada Mágica",
    loader: () => {
      return {
        content: [
          <p className="mb-4" key="step-4">
            Após vencer o dragão, Mah chega ao Salão de Valhala, onde uma
            celebração mágica acontece em sua honra. Merodin a parabeniza por
            sua coragem e habilidades gatófilas e oferece três prêmios, mas ele
            também conhece suas paixões:
          </p>,
        ],
        actions: [
          {
            text: "Uma varinha mágica que concede poderes inigualáveis de pintura.",
            goTo: "/final-1-a",
          },
          {
            text: "Um livro de feitiços antigos que expandirá seus conhecimentos mágicos.",
            goTo: "/final-1-b",
          },
          {
            text: "Um banquete mágico com todas as iguarias e poções que ela ama, ao lado de seu gato de estimação.",
            goTo: "/final-1-c",
          },
        ],
      };
    },
  },
  // Final 1
  {
    path: "/final-1-a",
    element: <GreatFinal />,
    loader: () => {
      return {
        content: [
          <p className="mb-4" key="final-1-a">
            A história termina com Mah, a Bruxinha Gatófila, celebrando seu
            aniversário em Valhala, com sua varinha mágica colorindo tudo,
            criando artes incríveis pelo mundo.
          </p>,
        ],
        actions: [
          {
            text: "Ir dormir...",
            goTo: "/dormir",
          },
          {
            text: "Ir Comer...",
            goTo: "/comer",
          },
        ],
      };
    },
  },
  {
    path: "/final-1-b",
    element: <GreatFinal />,
    loader: () => {
      return {
        content: [
          <p className="mb-4" key="final-1-b">
            A história termina com Mah, a Bruxinha Gatófila, celebrando seu
            aniversário em Valhala, com seu novo livro de feitiços e poções sem
            glutém, se deslumbrando com todas as possibilidades.
          </p>,
        ],
        actions: [
          {
            text: "Ir dormir...",
            goTo: "/dormir",
          },
          {
            text: "Ir Comer...",
            goTo: "/comer",
          },
        ],
      };
    },
  },
  {
    path: "/final-1-c",
    element: <GreatFinal />,
    loader: () => {
      return {
        content: [
          <p className="mb-4" key="final-1-c">
            A história termina com Mah, a Bruxinha Gatófila, celebrando seu
            aniversário em Valhala, enchendo os 7 buchos, com todas iguarias e
            poções que ela ama, com Leia ao seulado.
          </p>,
        ],
        actions: [
          {
            text: "Ir dormir...",
            goTo: "/dormir",
          },
          {
            text: "Ir Comer...",
            goTo: "/comer",
          },
        ],
      };
    },
  },
  // Final 2
  {
    path: "/final-2",
    element: <BadFinal />,
    loader: () => {
      return {
        content: [
          <p className="mb-4" key="final-2">
            Mah, desiste no meio do caminho pra tirar um cochilinho. Merodin
            guarda o presente especial para o próximo ano.
          </p>,
        ],
        actions: [
          {
            text: "Recomeçar",
            goTo: "/",
          },
        ],
      };
    },
  },
  // Final 3
  {
    path: "/final-3",
    element: <BadFinal />,
    loader: () => {
      return {
        content: [
          <p className="mb-4" key="final-4">
            Mah, recusa o chamado de Merodin, abraça Leia e volta a dormir.
            Merodin guarda o presente especial para o próximo ano.
          </p>,
        ],
        actions: [
          {
            text: "Recomeçar",
            goTo: "/",
          },
        ],
      };
    },
  },
  // End 1
  {
    path: "/comer",
    element: <End />,
    loader: () => {
      return {
        content: [
          <p className="mb-4" key="comer">
            NHAC NHAC NHAC ...
          </p>,
        ],
        actions: [
          {
            text: "Recomeçar",
            goTo: "/",
          },
        ],
      };
    },
  },
  // End 2
  {
    path: "/dormir",
    element: <End />,
    loader: () => {
      return {
        content: [
          <p className="mb-4" key="dormir">
            Z Z Z Z Z ...
          </p>,
        ],
        actions: [
          {
            text: "Recomeçar",
            goTo: "/",
          },
        ],
      };
    },
  },
]);

export default routers;
