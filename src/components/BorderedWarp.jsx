import PropTypes from "prop-types";

const BorderedWarp = (props) => {
  return (
    <div className="h-screen flex">
      <div className="flex flex-col flex-grow border-2 border-black mx-2 my-2">
        {props.children}
      </div>
    </div>
  );
};

BorderedWarp.propTypes = {
  children: PropTypes.node.isRequired,
};

export default BorderedWarp;
