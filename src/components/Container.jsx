import PropTypes from "prop-types";

const Container = (props) => {
  return <div className="container mx-auto">{props.children}</div>;
};

Container.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Container;
