import PropTypes from "prop-types";

const CenterView = (props) => {
  return (
    <div className=" flex-1 flex items-center justify-center">
      {props.children}
    </div>
  );
};

CenterView.propTypes = {
  children: PropTypes.node.isRequired,
};

export default CenterView;
