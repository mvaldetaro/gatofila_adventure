import { Button, CenterView } from "../components";

const Intro = () => {
  return (
    <CenterView>
      <div className="w-3/4 text-justify flex-col flex justify-center items-center gap-y-6">
        <div className="flex flex-col items-center">
          <p className="mb-4">
            Mah, uma pequena valquíria gatófila, está prestes a completar a
            maioridade, em seu 33º aniversário. Ela foi adotada por gatos ainda
            bebê, o que deu ela diversas habilidades gatiferas, como: julgar com
            olhar, ficar de mau humor quando está com fome, dormir bem
            dobradinha, dormir por longas horas, dormir após comer e dormir após
            dormir. Enquanto os preparativos para a comemoração começam, ela é
            chamada por Odin, o Pai de Todos Fura Bolo Mata Piolho, para uma
            missão em seu dia especial.
          </p>
        </div>
        <div className="flex-col flex justify-center items-center gap-y-2">
          <Button>Continuar</Button>
        </div>
      </div>
    </CenterView>
  );
};

export default Intro;
