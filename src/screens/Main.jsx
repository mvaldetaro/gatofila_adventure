import { memo } from "react";
import { CenterView } from "../components";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import routers from "../data/routers";

const router = createBrowserRouter(routers);

const Main = memo(() => {
  return (
    <CenterView>
      <RouterProvider router={router} />
    </CenterView>
  );
});

Main.displayName = "Main";

export default Main;
