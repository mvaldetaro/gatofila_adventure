import { Button, CenterView } from "../components";

const Step1 = () => {
  return (
    <CenterView>
      <div className="w-3/4 text-justify flex-col flex justify-center items-center gap-y-6">
        <div className="flex flex-col items-center">
          <p className="mb-4">
            Era uma vez, a muito tempo atrás, numa galaxia muito distante numa
            toca no chão vivia uma pequena valquíria. Não uma toca suja e úmida,
            cheia de restos de minhoca e cheiro de loudo, tampouco uma toca
            seca, vazia e arenosa, sem nada para sentar ou comer: era a toca de
            uma pequena valiquíria, e isso quer dizer aconchego.
          </p>
          <p>
            A toca tinha, lápis e pincéis de todas as cores. Pinturas e desenhos
            em todas as paredes, uma cama grande que caberia 42 dela e uma gata
            peluda com muita personalidade chamada Leia.
          </p>
        </div>
        <div className="flex-col flex justify-center items-center gap-y-2">
          <Button>Continuar</Button>
        </div>
      </div>
    </CenterView>
  );
};

export default Step1;
